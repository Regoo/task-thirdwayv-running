import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {
  ScrollView as NativeScrollView,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  RefreshControl,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';

import {
  BasePropTypes,
  dimensionsStyles,
  selfLayoutStyles,
  childrenLayoutStyles,
  backgroundColorStyles,
  paddingStyles,
  marginStyles,
  borderStyles,
} from './Base';

const styles = StyleSheet.create({
  rtl: {
    transform: [{scaleX: -1}],
  },
});

class ScrollView extends PureComponent {
  static propTypes = {
    ...BasePropTypes,
    horizontal: PropTypes.bool,
  };

  static defaultProps = {
    showVIndicator: false,
    showHIndicator: false,
    avoidPadding: 0,
  };

  constructor(props) {
    super(props);

    this.ref = React.createRef();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.topScroll) {
      this.refs._scrollView.scrollTo({x: 1, y: 1, animated: true});
    }
  }

  render() {
    const {
      horizontal,
      flattenChildren,
      flexGrow,
      rtl,
      style,
      contentStyle,
      row,
      showVIndicator,
      showHIndicator,
      avoidPadding,
      onRefresh,
      onScroll,
      decelerationRate,
      ...rest
    } = this.props;
    let {children} = this.props;
    if (children && !Array.isArray(children)) {
      children = [children];
    }

    if (flattenChildren) {
      children = children.reduce((acc, item) => acc.concat(item), []);
    }

    const h = horizontal || row;

    const main_node = (
      <NativeScrollView
        // ref={this.ref}
        ref="_scrollView"
        {...rest}
        showsVerticalScrollIndicator={showVIndicator}
        showsHorizontalScrollIndicator={showHIndicator}
        horizontal={horizontal}
        alwaysBounceVertical={false}
        right={null}
        left={null}
        flex={null}
        decelerationRate={decelerationRate ? 0 : undefined}
        snapToInterval={
          decelerationRate ? Dimensions.get('window').height : undefined
        }
        // snapToAlignment={'center'}
        contentInset={
          decelerationRate
            ? {
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
              }
            : undefined
        }
        style={[
          dimensionsStyles(rest),
          selfLayoutStyles(rest),
          backgroundColorStyles(rest),
          borderStyles(rest),
          marginStyles({...rest, row: h}),
          rtl && h ? styles.rtl : null,
          style,
        ]}
        contentContainerStyle={[
          !horizontal && childrenLayoutStyles(rest),
          paddingStyles({...rest, row: h}),
          flexGrow && {flexGrow: 1},
          contentStyle,
        ]}
        onScroll={
          onScroll
            ? (event) => {
                onScroll(event.nativeEvent);
              }
            : undefined
        }
        refreshControl={
          onRefresh ? (
            <RefreshControl
              refreshing={false}
              onRefresh={() => {
                onRefresh();
              }}
            />
          ) : undefined
        }>
        {React.Children.map(children, (child) =>
          child
            ? React.cloneElement(child, {
                style: [
                  Object.getDeepProp(child, 'props.style'),
                  rtl && h ? styles.rtl : {},
                ],
              })
            : child,
        )}
      </NativeScrollView>
    );

    if (Platform.OS === 'ios' && !h) {
      return (
        <KeyboardAvoidingView
          style={{alignSelf: 'stretch', flex: 1}}
          behavior="padding"
          enabled>
          {main_node}
        </KeyboardAvoidingView>
      );
    }

    return (
      <KeyboardAvoidingView
        style={{alignSelf: 'stretch', flex: 1, paddingBottom: avoidPadding}}
        behavior={null}
        enabled>
        {main_node}
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = (state) => ({
  rtl: state.lang.rtl,
});

export default connect(
  mapStateToProps,
  null,
  null,
  // { forwardRef: true }
)(ScrollView);
