/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {SafeAreaView, Platform, View} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import colors from './defaults/colors';

function Wrapper(props) {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  let {children, noSafeAreaView, saveColor} = props;
  return (
    <>
      {noSafeAreaView && Platform.OS === 'ios' ? (
        <View
          style={[
            {
              flex: 1,
              alignSelf: 'stretch',
              backgroundColor: saveColor ? saveColor : colors.statusBar,
            },
          ]}>
          {children}
        </View>
      ) : (
        <SafeAreaView
          style={[
            {
              flex: 1,
              alignSelf: 'stretch',
              backgroundColor: saveColor ? saveColor : colors.statusBar,
            },
          ]}>
          {children}
        </SafeAreaView>
      )}
    </>
  );
}

export default Wrapper;
