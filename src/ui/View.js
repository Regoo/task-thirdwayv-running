/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {PureComponent} from 'react';
import {
  View as RNView,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {RectButton} from 'react-native-gesture-handler';
import {connect} from 'react-redux';

import {
  BasePropTypes,
  dimensionsStyles,
  selfLayoutStyles,
  childrenLayoutStyles,
  backgroundColorStyles,
  elevationStyles,
  paddingStyles,
  marginStyles,
  borderStyles,
  borderRadiusStyles,
  overflowStyles,
} from './Base';
import {ref} from 'yup';

class View extends PureComponent {
  static propTypes = {
    ...BasePropTypes,
  };

  componentDidMount() {
    if (this.props.onRef) {
      this.props.onRef(this);
    }
  }
  render() {
    const {
      onLayout,
      style,
      onPress,
      touchableOpacity,
      children,
      width,
      height,
      equalSize,
      circleRadius,
      flex,
      flexInnerTouchable,
      stretch,
      stretchInnerTouchable,
      linearBackgroundGradient,
      disabled,
      colorLinear,
      noFlexPress,
    } = this.props;

    const Container = touchableOpacity ? TouchableOpacity : RectButton;

    const node = onPress ? (
      <Container
        // borderless={!linearBackgroundGradient}
        onPress={onPress}
        activeOpacity={disabled ? 1 : 0.1}
        style={[
          childrenLayoutStyles(this.props),
          overflowStyles(this.props),
          paddingStyles(this.props),
          {
            flex:
              (flex ||
                flexInnerTouchable ||
                height ||
                equalSize ||
                circleRadius) &&
              !noFlexPress
                ? 1
                : null,
            alignSelf:
              stretch ||
              stretchInnerTouchable ||
              width ||
              equalSize ||
              circleRadius
                ? 'stretch'
                : null,
          },
        ]}>
        {children}
      </Container>
    ) : (
      children
    );

    const ViewContainer = RNView;

    return (
      <ViewContainer
        onLayout={onLayout}
        {...(linearBackgroundGradient || {})}
        style={[
          dimensionsStyles(this.props),
          selfLayoutStyles(this.props),
          !onPress ? childrenLayoutStyles(this.props) : undefined,
          backgroundColorStyles(this.props),
          overflowStyles(this.props),
          !onPress ? paddingStyles(this.props) : undefined,
          marginStyles(this.props),
          borderStyles(this.props),
          borderRadiusStyles(this.props),
          elevationStyles(this.props),
          style,
        ]}
        delayPressIn={0}
        delayPressOut={0}
        disabled={true}>
        {node}
      </ViewContainer>
    );
  }
}

const mapStateToProps = state => ({
  rtl: state.lang.rtl,
});

export default connect(mapStateToProps)(View);
