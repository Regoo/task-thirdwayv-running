import {Dimensions, PixelRatio, Platform, StatusBar} from 'react-native';

const {roundToNearestPixel} = PixelRatio;

const decorateHeights = Platform.OS === 'android' ? StatusBar.currentHeight : 0;

export const APPBAR_HEIGHT = Platform.OS === 'ios' ? 54 : 56;

export const {width: windowWidth} = Dimensions.get('window');
export const windowHeight = Dimensions.get('window').height;

export const {width: screenWidth} = Dimensions.get('screen');
const [shortDimension, longDimension] =
  windowWidth < windowHeight
    ? [windowWidth, windowHeight]
    : [windowHeight, windowWidth];

export const screenHeight = Dimensions.get('screen').height - decorateHeights;
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;
export const scale = (size) => (shortDimension / guidelineBaseWidth) * size;
export const verticalScale = (size) =>
  (longDimension / guidelineBaseHeight) * size;

export const aspectRatio = () => windowHeight / windowWidth;

// export const responsiveWidth = w =>
//   roundToNearestPixel(Math.min(maxWidth, windowWidth) * (w / 100));

// export const responsiveHeight = h =>
//   roundToNearestPixel(Math.min(maxHeight, windowHeight) * (h / 100));

export const responsiveWidth = (w) => windowWidth * (w / 100);

export const responsiveHeight = (h) => windowHeight * (h / 100);

export const heightPercent = (h) =>
  parseFloat(((h / windowHeight) * 100).toFixed(2));

export const moderateScale = (size, factor = 0.5) => {
  const rw = Math.min(windowWidth, windowWidth) * (size / 100);

  return roundToNearestPixel(size + (rw - size) * factor);
};
export const moderateScaleW = (size, factor = 0.5) => {
  return size + (scale(size) - size) * factor;
};

export const moderateScaleH = (size, factor = 0.5) => {
  return size + (verticalScale(size) - size) * factor;
};

export const responsiveFontSize = (f, factor = 0.5) => {
  const rw = Math.min(windowWidth, windowWidth) * ((f-.5) / 100);

  return roundToNearestPixel((f-.5) + (rw - (f-.5)) * factor);
};

//export { windowWidth, windowHeight, screenWidth, screenHeight };
