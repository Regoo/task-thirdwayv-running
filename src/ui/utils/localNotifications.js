/* eslint-disable prettier/prettier */
import {RNToasty} from 'react-native-toasty';
import {responsiveFontSize} from './responsiveDimensions';

export const showInfo = (message) => {
  RNToasty.Info({title: message, titleSize: responsiveFontSize(6)});
};

export const showSuccess = (message) => {
  RNToasty.Success({title: message, titleSize: responsiveFontSize(6)});
};

export const showError = (message) => {
  RNToasty.Error({title: message, titleSize: responsiveFontSize(6)});
};
