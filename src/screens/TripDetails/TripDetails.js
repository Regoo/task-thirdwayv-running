import React, {useState, useEffect, useRef} from 'react';
import {Wrapper, View, Text} from '../../ui';
import I18n from 'react-native-i18n';
import {useSelector, useDispatch} from 'react-redux';
import Header from '../../components/Header';
import moment from 'moment';
import MapView, {Marker, PROVIDER_GOOGLE, Polygon} from 'react-native-maps';
import {StyleSheet, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

function TripDetails(props) {
  let {data} = props;
  const {lang} = useSelector(state => ({
    lang: state.lang.lang,
  }));
  const dispatch = useDispatch();
  const [state, setstate] = useState();
  const mapRef = useRef('mapRef');
  useEffect(() => {
    return () => {};
  }, []);
  return (
    <Wrapper>
      <View flex stretch backgroundColor="bg">
        <Header title={I18n.t('Trip details')} />
        <View flex stretch>
          <View m={10} borderRadius={10} p={10} stretch bw={2} bc="primary">
            <View row>
              <Text size={6} color="#FFC600">{`${I18n.t(
                'Start time',
              )}: `}</Text>
              <Text size={6}>
                {moment(data.startDate)
                  .locale(lang)
                  .format('dddd, DD-MM-YYYY hh:mm A')}
              </Text>
            </View>
            <View row mt={5}>
              <Text size={6} color="#FFC600">{`${I18n.t('End time')}: `}</Text>
              <Text size={6}>
                {moment(data.endDate)
                  .locale(lang)
                  .format('dddd, DD-MM-YYYY hh:mm A')}
              </Text>
            </View>
            <View row mt={5}>
              <Text size={6} color="#FFC600">{`${I18n.t('Distance')}: `}</Text>
              <Text size={6}>
                {`${parseFloat(data.distance).toFixed(2)} ${I18n.t('meter')}`}
              </Text>
            </View>
            <View row mt={5}>
              <Text size={6} color="#FFC600">{`${I18n.t(
                'Number of steps',
              )}: `}</Text>
              <Text size={6}>{data.steps}</Text>
            </View>
          </View>
          <View flex stretch>
            <MapView
              style={{...StyleSheet.absoluteFillObject}}
              provider={PROVIDER_GOOGLE}
              region={
                Array.isArray(data.tripPoint) && data.tripPoint.length !== 0
                  ? {
                      ...data.tripPoint[0],
                      latitudeDelta: LATITUDE_DELTA,
                      longitudeDelta: LONGITUDE_DELTA,
                    }
                  : {
                      latitude: 30.0446504,
                      longitude: 31.2367137,
                      latitudeDelta: LATITUDE_DELTA,
                      longitudeDelta: LONGITUDE_DELTA,
                    }
              }
              ref={mapRef}
              // mapType="satellite"
            >
              {Array.isArray(data.tripPoint) && data.tripPoint.length !== 0 ? (
                <Polygon
                  coordinates={data.tripPoint}
                  fillColor="rgba(0, 200, 0, 0.5)"
                  strokeColor="rgba(32, 99, 155,0.5)"
                  strokeWidth={5}
                />
              ) : null}
            </MapView>
          </View>
        </View>
      </View>
    </Wrapper>
  );
}
export default TripDetails;
