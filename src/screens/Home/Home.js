/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {Wrapper, Navigation, View, Text} from '../../ui';
import {useSelector, useDispatch} from 'react-redux';
import I18n from 'react-native-i18n';
import Header from '../../components/Header';

import GoogleFit, {Scopes} from 'react-native-google-fit';
import {showError, showSuccess} from '../../ui/utils/localNotifications';
import Geolocation from 'react-native-geolocation-service';
import {PERMISSIONS, request} from 'react-native-permissions';
import {ActivityIndicator, Platform, Dimensions} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import Geolocation1 from '@react-native-community/geolocation';
import AsyncStorage from '@react-native-community/async-storage';
import LottieView from 'lottie-react-native';
const {width, height} = Dimensions.get('screen');
import {setLang} from '../../actions/lang';

function Home(props) {
  const {rtl} = useSelector(state => ({
    rtl: state.lang.rtl,
  }));
  let dispatch = useDispatch();
  const [start, setStart] = useState(false);
  const [androidVersion, setAndroidVersion] = useState(false);
  const [recordData, setRecordData] = useState({
    startDate: '',
    endDate: '',
  });
  const [tripPoint, setTripPoint] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (Platform.OS === 'android') {
      let parts = DeviceInfo.getSystemVersion().split('.');
      if (parseInt(parts[0]) >= 11) {
        setAndroidVersion(true);
      }
    }
    return () => {
      if (start) {
        endTrip();
      }
    };
  }, []);

  let getPermissionGoogleFit = async () => {
    let getPermission = false;
    const options = {
      scopes: [
        Scopes.FITNESS_ACTIVITY_READ,
        Scopes.FITNESS_ACTIVITY_WRITE,
        Scopes.FITNESS_BODY_READ,
        Scopes.FITNESS_BODY_WRITE,
        Scopes.FITNESS_LOCATION_READ,
        Scopes.FITNESS_LOCATION_WRITE,
      ],
    };

    await GoogleFit.authorize(options)
      .then(authResult => {
        console.log('authResult ', authResult);
        if (authResult.success) {
          _requestPermission();
          getPermission = true;
        } else {
          showError(I18n.t('An error occurred while giving permissions'));
          setLoading(false);
          setStart(false);
        }
      })
      .catch(() => {
        showError(I18n.t('An error occurred while giving permissions'));
        setLoading(false);
        setStart(false);
      });
    return getPermission;
  };

  let getLatLng = async () => {
    await Geolocation.getCurrentPosition(
      position => {
        const {latitude, longitude} = position.coords;
      },
      error1 => {
        showError(I18n.t('Something went wrong You must open GPS'));
        setLoading(false);
      },
      {enableHighAccuracy: false, timeout: 15000, maximumAge: 10000},
    );
    Geolocation1.getCurrentPosition(
      position => {
        const {latitude, longitude} = position.coords;
        startTrip({latitude, longitude});
      },
      error1 => {
        showError(I18n.t('Something went wrong You must open GPS'));
        setLoading(false);
      },
      {enableHighAccuracy: false, timeout: 15000, maximumAge: 10000},
    );
  };

  let watchID = '';

  let getGeoLocation = async () => {
    watchID = Geolocation.watchPosition(
      position => {
        console.log('watchPosition ', position);
        const {latitude, longitude} = position.coords;

        setTripPoint([...tripPoint, {latitude, longitude}]);
      },
      error => {},
      {enableHighAccuracy: false, timeout: 15000, maximumAge: 10000},
    );
  };

  let _requestPermission = async () => {
    let permission =
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
        : PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION;
    await request(permission).then(async result => {
      if (result === 'granted') {
        getLatLng();
      } else {
        setLoading(false);
        showError(I18n.t('An error occurred while giving permissions'));
      }
    });
  };

  let startJogging = () => {
    setLoading(true);
    getPermissionGoogleFit();
  };

  let startTrip = async location => {
    setTripPoint([...tripPoint, location]);
    getGeoLocation();
    if (Platform.OS === 'android' && androidVersion) {
      await request(PERMISSIONS.ANDROID.ACTIVITY_RECOGNITION).then(
        async result => {
          if (result === 'granted') {
            setRecordData({startDate: new Date()});
            setStart(true);
            GoogleFit.startRecording(callback => {
              setLoading(false);
              // Process data from Google Fit Recording API (no google fit app needed)
            });
            showSuccess(I18n.t("Let's start walking"));
          } else {
            setLoading(false);
            showError(I18n.t('An error occurred while giving permissions'));
          }
        },
      );
    } else {
      setRecordData({startDate: new Date()});
      setStart(true);
      GoogleFit.startRecording(callback => {
        setLoading(false);
        // Process data from Google Fit Recording API (no google fit app needed)
      });
      showSuccess(I18n.t("Let's start walking"));
      GoogleFit.observeSteps(callback => {
        setLoading(false);
      });
    }
  };
  let endTrip = () => {
    setLoading(true);
    if (watchID) {
      Geolocation.clearWatch(watchID);
    }
    const opt = {
      startDate: new Date(recordData.startDate).toISOString(), // required ISO8601Timestamp
      endDate: new Date().toISOString(), // required ISO8601Timestamp
      // bucketUnit: BucketUnit.DAY, // optional - default "DAY". Valid values: "NANOSECOND" | "MICROSECOND" | "MILLISECOND" | "SECOND" | "MINUTE" | "HOUR" | "DAY"
      // bucketInterval: 1, // optional - default 1.
    };
    let record = {...recordData, endDate: new Date()};
    GoogleFit.getDailyStepCountSamples(opt)
      .then(res => {
        let filtered = res.filter(
          item => item.source === 'com.google.android.gms:estimated_steps',
        );
        if (filtered.length !== 0 && filtered[0].steps.length !== 0) {
          record.steps = filtered[0].steps[0].value;
          GoogleFit.getDailyDistanceSamples(opt).then(res => {
            if (res.length !== 0) {
              record.distance = res[0].distance;
              setRecordData(record);
              logRecordData(record);
            } else {
              record.distance = 0;
              setRecordData(record);
              logRecordData(record);
            }
            setLoading(false);
          });
        } else {
          record.steps = 0;
          record.distance = 0;
          setRecordData(record);
          logRecordData(record);
          setLoading(false);
        }
      })
      .catch(err => {
        record.steps = 0;
        record.distance = 0;
        setRecordData(record);
        logRecordData(record);
        setLoading(false);
        console.warn(err);
      });
  };

  let logRecordData = async record => {
    setStart(false);
    setLoading(false);
    record.tripPoints = tripPoint;
    let logs = await AsyncStorage.getItem('Logs');
    if (logs) {
      let data = JSON.parse(logs);
      data = [record, ...data];
      await AsyncStorage.setItem('Logs', JSON.stringify(data));
    } else {
      let data = [record];
      await AsyncStorage.setItem('Logs', JSON.stringify(data));
    }
    setRecordData({startDate: '', endDate: ''});
    setTripPoint([]);
  };

  return (
    <Wrapper>
      <View flex stretch backgroundColor="bg">
        <Header
          title={I18n.t('Home')}
          hideBack
          rightItem={
            <Text
              color="white"
              mr={5}
              onPress={() => {
                dispatch(rtl ? setLang('en', false) : setLang('ar', true));
              }}>
              {rtl ? 'EN' : 'AR'}
            </Text>
          }
        />
        <View flex stretch p={10} center>
          {start ? (
            <LottieView
              source={require('../../assets/walk.json')}
              autoPlay
              loop
              style={{
                width: width * 0.3,
                height: height * 0.3,
              }}
            />
          ) : (
            <View
              stretch
              backgroundColor="primary"
              borderRadius={10}
              p={10}
              center
              touchableOpacity
              mb={10}
              onPress={() => {
                Navigation.push('Logs');
              }}>
              <Text>{I18n.t('Logs')}</Text>
            </View>
          )}
          <View
            stretch
            backgroundColor="primary"
            borderRadius={10}
            p={10}
            center
            touchableOpacity
            onPress={() => {
              loading ? null : start ? endTrip() : startJogging();
            }}>
            {loading ? (
              <ActivityIndicator color="white" size="small" />
            ) : (
              <Text>{I18n.t(start ? 'Jogging stop' : 'Jogging start')}</Text>
            )}
          </View>
        </View>
      </View>
    </Wrapper>
  );
}

export default Home;
