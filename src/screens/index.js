import {Navigation} from '../ui';
import Home from './Home/Home';
import Logs from './Logs/Logs';
import TripDetails from './TripDetails/TripDetails';

export default function () {
  Navigation.registerScreen('Home', Home);
  Navigation.registerScreen('Logs', Logs);
  Navigation.registerScreen('TripDetails', TripDetails);
}
