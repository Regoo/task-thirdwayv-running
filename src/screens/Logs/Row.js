/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable radix */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {Text, View, Icon, Navigation} from '../../ui';
import {connect, useSelector} from 'react-redux';
import I18n from 'react-native-i18n';
import moment from 'moment';

function Row(props) {
  let {data, index} = props;
  const {lang} = useSelector(state => ({
    lang: state.lang.lang,
  }));

  return (
    <View
      m={10}
      borderRadius={10}
      p={10}
      stretch
      bw={2}
      bc="primary"
      touchableOpacity
      onPress={() => {
        Navigation.push({name: 'TripDetails', passProps: {data}});
      }}>
      <View row>
        <Text size={6} color="#FFC600">{`${I18n.t('Start time')}: `}</Text>
        <Text size={6}>
          {moment(data.startDate)
            .locale(lang)
            .format('dddd, DD-MM-YYYY hh:mm A')}
        </Text>
      </View>
      <View row mt={5}>
        <Text size={6} color="#FFC600">{`${I18n.t('End time')}: `}</Text>
        <Text size={6}>
          {moment(data.endDate).locale(lang).format('dddd, DD-MM-YYYY hh:mm A')}
        </Text>
      </View>
    </View>
  );
}

export default connect(null, {})(Row);
