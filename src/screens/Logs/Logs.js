import React, {useState, useEffect} from 'react';
import {Wrapper, View, Text} from '../../ui';
import I18n from 'react-native-i18n';
import {useSelector, useDispatch} from 'react-redux';
import Header from '../../components/Header';
import {FlatList, Dimensions} from 'react-native';
import Row from './Row';
import AsyncStorage from '@react-native-community/async-storage';
import LottieView from 'lottie-react-native';
const {width, height} = Dimensions.get('screen');

function Logs(props) {
  const {} = useSelector(state => ({}));
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    getData();
    return () => {};
  }, []);
  let getData = async () => {
    let logs = await AsyncStorage.getItem('Logs');
    if (logs) {
      let data1 = JSON.parse(logs);
      setData(data1);
      setLoading(false);
    } else {
      setLoading(false);
    }
  };
  return (
    <Wrapper>
      <View flex stretch backgroundColor="bg">
        <Header title={I18n.t('Logs')} />
        {loading || data.length === 0 ? (
          <View stretch flex center>
            {loading ? (
              <LottieView
                source={require('../../assets/loading1.json')}
                autoPlay
                loop
                style={{
                  width: width * 0.3,
                  height: height * 0.3,
                }}
              />
            ) : (
              <Text size={12}>{I18n.t('No logs yet')}</Text>
            )}
          </View>
        ) : (
          <View stretch flex>
            <FlatList
              data={data}
              style={{
                flex: 1,
                width: '100%',
              }}
              renderItem={({item, index}) => <Row data={item} index={index} />}
              keyExtractor={item => item.startDate}
            />
          </View>
        )}
      </View>
    </Wrapper>
  );
}
export default Logs;
