import {Navigation} from 'react-native-navigation';
import axios from 'axios';
import store from './store';
import registerScreens from './screens';
import {Navigation as nv} from './ui';
import {initInternetConnection} from './actions/network';
import {initLang} from './actions/lang';
import moment from 'moment';

export const startApp = () => {
  registerScreens();
  axios.interceptors.request.use(
    config => {
      const {token} = store.getState().auth;
      const {lang} = store.getState().lang;
      return {
        ...config,
        headers: {
          ...config.headers,
          'Accept-Language': lang,
          Authorization: token
            ? `Bearer ${token}`
            : config.headers.Authorization,
        },
      };
    },

    error => {
      Promise.reject(error);
    },
  );

  moment.locale('en');
  Navigation.events().registerAppLaunchedListener(async () => {
    nv.setNavigationDefaultOptions();
    initInternetConnection(store.dispatch);
    await initLang('ar', true)(store.dispatch);

    nv.init(
      'MAIN_STACK',
      {
        rtl: store.getState().lang.rtl,
        name: 'Home',
      },
      false,
    );
  });
};
