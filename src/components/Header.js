/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Platform} from 'react-native';
import {View, Text, Navigation, Icon} from '../ui';
import {useSelector} from 'react-redux';

function Header(props) {
  const {rtl} = useSelector(state => ({
    rtl: state.lang.rtl,
  }));
  const {
    backgroundColor,
    title,
    tc,
    showMenu,
    onBack,
    hideBack,
    rightItem,
    subTitle,
    MenuPress,
  } = props;
  let goBack = () => {
    Navigation.pop();
  };

  let renderLeft = () => {
    return (
      <View row>
        {!hideBack && (
          <Icon
            pv={5}
            ph={2}
            flip
            name={Platform.OS === 'ios' ? 'arrow-back-ios' : 'arrow-back'}
            type={'MaterialIcons'}
            size={12}
            color={tc ? tc : 'secondary'}
            onPress={onBack ? () => onBack() : goBack}
          />
        )}
        {showMenu && (
          <Icon
            pv={5}
            ph={2}
            flip
            name={'menu'}
            type={'Feather'}
            size={12}
            color={tc ? tc : 'secondary'}
            onPress={
              MenuPress
                ? () => {
                    MenuPress();
                  }
                : ()=>{
                  Navigation.openMenu();
                }
            }
          />
        )}
      </View>
    );
  };

  let renderRight = () => {
    return rightItem ? rightItem : <View row />;
  };

  return (
    <View
      stretch
      backgroundColor={backgroundColor ? backgroundColor : 'primary'}>
      <View stretch row spaceBetween ph={5} pv={15}>
        {renderLeft()}
        <View flex stretch center>
          <Text
            color={tc ? tc : 'secondary'}
            size={8}
            style={{textAlign: 'center'}}>
            {title}
          </Text>
          {subTitle ? (
            <Text color={'#9C9C9C'} style={{textAlign: 'center'}}>
              {subTitle}
            </Text>
          ) : null}
        </View>
        <View>{renderRight()}</View>
      </View>
    </View>
  );
}

export default Header;
