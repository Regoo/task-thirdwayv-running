import langReducer from './lang';
import networkReducer from './network';
import {combineReducers} from 'redux';

export default combineReducers({
  
  lang: langReducer,
  network: networkReducer,
  
});
